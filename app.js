const { MongooseStartup} = require('./infra/mongoose.startup')
const { ExpressStartup } = require('./infra/express.startup')
const { ServerStartup } = require('./infra/server.startup')
/**
 * Função que inicia o nosso APP, chamando sequenciamente:
 * @type MongooseStartup
 * @type ExpressStartup
 * @type ServerStartup
 * 
 */
var initApp = async () => {
    return Promise.resolve()
    .then(() => new MongooseStartup().startup()) //Passo 1, entra na função e vê o que acontece
    .then(() => new ExpressStartup().startup()) //Passo 2
    .then((express) => new ServerStartup().startup(express)) //Passo 3
    .catch((err) => {
        console.error(`[ERROR] - Erro ao iniciar o APP`, err)
    })
}

initApp();