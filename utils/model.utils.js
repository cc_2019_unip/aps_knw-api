class ModelUtils{
    /**
     * Apagamos as propriedades:
     * createdAt, updatedAt e __v
    */
    static deleteDefaults(obj, ret){
        var mRet = ret;
        if(!!mRet && !!mRet["_id"])
            delete mRet["_id"]
        if(!!mRet && !!mRet.createdAt)
            delete mRet.createdAt
        if(!!mRet && !!mRet.updatedAt)
            delete mRet.updatedAt            
        if(!!mRet && typeof mRet.__v != 'undefined')
            delete mRet.__v    
            
        return ret;    
    }

    /** Configurações padrões que vão nas models */
    static defaultOptions() { 
        return {
            timestamps: true, 
            toJSON: {
                transform: ModelUtils.deleteDefaults
            }
        }
    }
}

module.exports = ModelUtils