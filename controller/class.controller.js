const express = require('express')
const router = express.Router()
const authMiddleware = require('../middlewares/auth.middleware')
const ClassModel = require('../model/class.model');

router.get('/', async (req, res) => {
    try {
        const classes = await ClassModel.find();

        return res.send({ classes });

    } catch (err) {
        return res.status(400).send({ error: "Cannot find Classes!" });
    }
});

router.get('/:cid', async (req, res) => {
    try {
        const cid = req.params.cid

        let classInfo = await ClassModel.findOne({ cid });

        return res.send({ classInfo });

    } catch (err) {
        return res.status(400).send({ error: "Cannot find Class!" });
    }
});

router.post('/', async (req, res) => {
    try {
        const classes = await ClassModel.create(req.body)
        return res.status(201).json(classes)

    } catch (err) {
        return res.status(400).send({ error: "Error creating new project" });
    }
})

router.put('/:cid', async (req, res) => {
    try {
        const cid = req.params.cid;

        const classes = await ClassModel.findOneAndUpdate(cid, req.body, { new: true, runValidators: true });

        //colocar regra do populate students

        return res.send({ classes })

    } catch (err) {
        return res.status(400).send({ error: "Error Updating Class!" });
    }
})

router.delete('/:cid', async (req, res) => {
    try {
        const cid = req.params.cid

        let classes = await ClassModel.findOneAndRemove({ cid });

        return res.send({ classes });

    } catch (err) {
        return res.status(400).send({ error: "Error Remove the Class!" });
    }
})

module.exports = app => app.use('/class', authMiddleware, router);

