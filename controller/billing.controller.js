const express = require('express'),
    router = express.Router(),
    authMiddleware = require('../middlewares/auth.middleware'),
    BillingModel = require('../model/billing.model'),
    moment = require('moment');

//#region [METHODS]

/**
 * Busca os billing(s) através do Id ou um array de Ids
 * @param {Number | Number[]} id
 */
const getBillings = (req, res, next) => {
    let bid = !!req.query && typeof req.query.bid !== 'undefined' ? req.query.bid : null;

    const _validateData = (bid) => {
        let ret = [];

        //Veifica se não é um array
        if (!Array.isArray(bid))
            bid.push(bid)

        ret = bid.reduce((arr, item) => {
            var parsed = parseInt(item);
            if (Number.isInteger(parsed))
                arr.push(parsed);

            return arr;
        }, [])

        return Promise.resolve(ret);
    }

    const _findBilling = (bid) => {
        //Caso venha bid na requisição
        if (bid.length > 0)
            return BillingModel.find({ bid: { $in: bid } }, (err, billing) => {
                if (err) Promise.reject(`Houve um erro na busca \n${err}`)
                return Promise.resolve(billing);
            })

        return Promise.resolve([])
    }
    return Promise.resolve(bid)
        .then(_validateData)
        .then(_findBilling)
        .then((ret) => res.status(200).json(ret))
        .catch((err) => {
            console.error(err);
            res.status(500)
            return next();
        })
}

/**
 * Busca os billing(s) e deleta através do Id ou um array de Ids
 * @param {Number | Number[]} id
 */
const deleteBillings = (req, res, next) => {
    let bid = !!req.query && typeof req.query.bid !== 'undefined' ? req.query.bid : null;
    const _validate = (bid) => {
        let ret = [];

        //Veifica se não é um array
        if (!Array.isArray(bid))
            ret.push(bid)

        ret = bid.reduce((arr, item) => {
            var parsed = parseInt(item);
            if (Number.isInteger(parsed))
                arr.push(parsed);

            return arr;
        }, [])

        return Promise.resolve(ret);
    }

    const _deleteBilling = (ids) => {
        return BillingModel.deleteMany({ bid: { $in: ids } }, (err) => {
            if (err) Promise.reject(`Houve um erro para deletar \n${err}`)
            return Promise.resolve(true)
        })
    }

    return Promise.resolve(bid)
        .then(_validate)
        .then(_deleteBilling)
        .then((ret) => res.status(200).json(ret))
        .catch((err) => {
            console.error(err);
            res.status(500)
            return next();
        })
}

/**
 * Faz a inserção de billing
 * @param {Number} paidedAt
 * @param {Number} studentId
 * @param {Number} price
 * @param {Date} billingDate
 */
const createBilling = async (req, res, next) => {
    try {
        if (!!req.body) {
            var data = { ...req.body };

            var doc = await BillingModel.create(data)

            if (!doc)
                throw new Error('Problemas ao salvar')

            return res.json(doc.toJSON())
        }
    }
    catch (err) {
        return res.status(500).send(err.message);
    }
}

/**
 * Faz a atualição de billing a partir do id
 * @param {Number} bid
 * @param {Number} paidedAt
 * @param {Number} studentId
 * @param {Number} price
 * @param {Date} billingDate
 */
const updateBilling = async (req, res, next) => {
    try {
        if (!!req.body) {
            var data = { ...req.body };

            var doc = await BillingModel.findOneAndUpdate({bid: data.bid}, data, {runValidators: true})

            if (!doc)
                throw new Error('Problemas ao salvar')

            return res.json(doc.toJSON())
        }
    }
    catch (err) {
        console.error(err)
        return res.status(500).send(err.message);
    }
}
//#endregion

router.get('/', getBillings)
router.delete('/', deleteBillings)
router.post('/', createBilling)
router.put('/', updateBilling)

module.exports = app => app.use('/billing', authMiddleware, router);