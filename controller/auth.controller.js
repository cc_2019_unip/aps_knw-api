const express = require('express');
const User = require('../model/user.model.js');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const authConfig = require('../config/auth.config');
const moment = require('moment');

function generateToken(params = {}) {
    let expireTime = 86400;
    let token = jwt.sign(params, authConfig.secret, {
        expiresIn: expireTime,
    })
    return {
        token, 
        expiresIn: moment().add(expireTime, 'seconds').toISOString(true)
    }

}

router.post('/register', async (req, res) => {
    const { email } = req.body
    try {
        if (await User.findOne({ email }))
            return res.status(400).send({ error: 'User already exists' });

        let salt = await bcrypt.genSaltSync(10);
        let hash = await bcrypt.hashSync(String(req.body.password), salt);
        req.body.password = hash;

        let user = (await User.create(req.body)).toJSON();
        delete user.password
        
        let credentials = generateToken({ id: user.id });
        return res.send({
            user,
            token: credentials.token,
            expiresIn: credentials.expiresIn
        });
    } catch (err) {
        console.error(err)
        return res.status(400).send({ error: 'Registration failed!' })
    }
});

router.post('/', async (req, res) => {
    const { email, password } = req.body
    if(!email || !password)
        return res.status(400).json({error: 'Missing e-mail or password!'})

    var user = await User.findOne({ email })

    if(!user)
        return res.status(400).json({error: 'User not found'})

        
    if (!user)
        return res.status(400).json({ error: 'User not Found!' });
        
    if (!await bcrypt.compare(password, String(user.password),))
        return res.status(400).json({ error: 'Invalid Password!' });
        
    let ret = {
        email: user.toJSON().email,
        name: user.toJSON().name,
    };
    let credentials = generateToken({ id: user.id });
    res.json({
        ret,
        token: credentials.token,
        expiresIn: credentials.expiresIn
    })
});

module.exports = app => app.use('/auth', router);