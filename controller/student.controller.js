const express = require("express");
const router = express.Router();
const StudentModel = require("../model/student.model");
const authMiddleware = require('../middlewares/auth.middleware')

router.use(authMiddleware);

router.get('/', async (req, res, next) => {
    try {
        const students = await StudentModel.find();

        return res.send({ students });

    } catch (err) {
        return res.status(400).send({ error: "Cannot find Students!" });
    }
})

router.get("/:sid", async function (req, res, next) {
    try {
        const sid = req.params.sid

        let studentInfo = await StudentModel.findOne({ sid });

        return res.send(studentInfo);
    }
    catch{
        return res.status(400).send({ error: "Cannot find Student!" });
    }

})
router.post('/', async (req, res) => {
    try {

        let cpf = req.body.cpf

        let student = await StudentModel.findOne({ cpf });

        if(student) return res.status(401).send("CPF já cadastrado!")

        // if (req.body.rg.length != 9) return res.status(400).send({ error: "Cannot insert student RG is invalid!" });

        // if (req.body.cpf.length != 11) return res.status(400).send({ error: "Cannot insert student RG is invalid!" });

        const students = await StudentModel.create(req.body);

        res.send({ students })

    } catch (err) {
        res.status(400).send({ error: err.errors });
    }
})

router.put("/:sid", async (req, res, next) => {
    try {
        const sid = req.params.sid;
        let body = req.body
        const students = await StudentModel.findOneAndUpdate(sid, body, { new: true, runValidators: true });


        return res.send({ students })

    } catch (err) {
        return res.status(400).send({ error: err.errors });
    }

})
router.delete("/:sid", async (req, res, next) => {

    try {
        const sid = req.params.sid

        let students = await StudentModel.findOneAndDelete({ sid });

        return res.send({ students });

    } catch (err) {
        return res.status(400).send({ error: "Error Remove the Class!" });
    }
});

module.exports = app => app.use('/students', authMiddleware, router);