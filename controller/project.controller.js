const express = require('express');
const authMiddleware = require('../middlewares/auth.middleware');
const router = express.Router();

router.use(authMiddleware);

router.get('/', (req, res) => {
    res.json({ ok: true });
});

module.exports = app => app.use('/test', router);