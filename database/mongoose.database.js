const mongoose = require('mongoose');
const { defaultOptions } = require('../utils/model.utils')

class Schema extends mongoose.Schema{
    constructor(args){
        super(args, defaultOptions());
    }
}

exports.Schema = Schema;