const express = require('express'),
    bodyParser = require('body-parser'),
    cors = require('cors');


class ExpressStartup {
    /** Configurar os middlewares padrões, como:
     * @example - cors, bodyparser...
     */
    _setupMiddlewares(app) {
        app.use(cors())
        app.use(bodyParser.json())
        app.use(bodyParser.urlencoded({ extended: false }));
        return Promise.resolve(app)
    }

    /** Aqui importamos as rotas */
    _setupRoutes(app) {
        require('../controller/auth.controller')(app)
        require('../controller/project.controller')(app)
        require('../controller/student.controller')(app)
        require('../controller/billing.controller')(app)
        require('../controller/class.controller')(app)

        return Promise.resolve(app)
    }

    constructor() {
        this.app = express();
    }

    /** Configura o express, como as bibliotecas de middleware e as rotas */
    startup() {
        return Promise.resolve(this.app)
            .then(this._setupMiddlewares)
            .then(this._setupRoutes)
            .catch(err => {
                return Promise.reject(err);
            })
    }
}

exports.ExpressStartup = ExpressStartup;