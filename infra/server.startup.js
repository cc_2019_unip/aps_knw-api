const defaultConfig = {
    port: process.env.PORT || 8080
}

class ServerStartup {
    /** Iniciamos o servidor adicionando os handles necessários */
    constructor(config = null){
        this.config = !!config ? config : defaultConfig;
    }
    
    /** Abre o servidor de fato, com a porta configurada */
    startup(app){
        var server = app.listen(this.config)
        .on('listening', () => {
            console.log(`[LISTENING] - Servidor iniciado: ${server.address().address}:${server.address().port}`)  
        })
        .on('connection', (socket) => {
            console.log(`[CONNECTION] - Nova conexão: ${socket.remoteAddress}:${socket.remotePort}`)
        })
        .on('error', (err) => {
            console.error(`[ERROR] - ${err}`)
        })
        .on('close', () => {
            console.warn(`[WARNING] - Fechando conexão...`)
        })
        return Promise.resolve()
    }
}

exports.ServerStartup = ServerStartup;