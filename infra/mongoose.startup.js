const mongoose = require('mongoose');

const defaultConfig = {
    url: 'mongodb://knwdb_106:q4y0ANCWyEybBYg+QEINYQ==@ds135876.mlab.com:35876/heroku_95gcklx6'
    // url: 'mongodb://localhost:27017/api-knw'
}

class MongooseStartup{
    /** Futuramente iremos buscar as configurações padrões pelo construtor */
    constructor(config = null){
        this.config = !!config ? config : defaultConfig;
    }
    
    /** Inicia conexão com o mongo, e escuta se acontecer algum erro emitindo log */
    startup(){
        return mongoose
        .connect(this.config.url, 
        { useNewUrlParser: true })
        .then((connection) => {
            console.log('[CONNECTION] - Mongo connected')
            return Promise.resolve()
        })
        .catch((err) => {
            return Promise.reject(`[ERROR] - Mongo connection problem: \n${err}`)
        });
    }
}

exports.MongooseStartup = MongooseStartup;