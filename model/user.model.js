const mongoose = require('mongoose');
const { Schema } = require('../database/mongoose.database')
const { MongooseAutoIncrementID } = require('mongoose-auto-increment-reworked');

const UserSchema = new Schema({
    /** Nome do usuário
     * @example name: "Matheus N. Correia"
     */
    name: {
        type: String,
        required: false,
        trim: true,
        maxlength: 255
    },
    /** Campo de e-mail do usuário que utilizará o sistema */
    email: {
        type: String,
        required: true,
        lowercase: true,
        trim: true
        //unique: true -> Isso é pra indexação
    },
    /** Vamos criar um esquema depois de criptografia na senha + salt(chave privada) */
    password: {
        type: String,
        required: false,
        select: true //-> Pra que serve isso? lol
    },
    /**
     * @important Este campo não deve retornar fora da API!
     */
    loggedAt: {
        type: Date,
        required: false
    }
    // createAt: { -> Não precisa desse campo porque adicionei a opção {timestamps: true}    
    //     type: Date,
    //     default: Date.now
    // }
    /** @see https://mongoosejs.com/docs/guide.html#timestamps */
})  

UserSchema.plugin(MongooseAutoIncrementID.plugin, {modelName: 'Users', unique: true, field: 'uid'})

const User = mongoose.model('Users', UserSchema)
module.exports = User;