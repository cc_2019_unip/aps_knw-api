const mongoose = require('mongoose')
const { SchemaTypes} = require('mongoose')
const { Schema } = require('../database/mongoose.database')
const { defaultOptions } = require('../utils/model.utils')
const { MongooseAutoIncrementID } = require('mongoose-auto-increment-reworked');

const BillingSchema = new Schema({
    /** 
     * Id do estudante associado
     * Olhem esse link de como usar na busca já populando o objeto:
     * @see https://mongoosejs.com/docs/populate.html#population
     */
    studentId:{
        min: 0,
        type: Number,
        required: true,
        ref: 'Students'
    },
    /** Preço do valor a ser cobrado */
    price: {
        min: 0,
        type: Number,
        required: true
    },
    /** Data da cobrança */
    billingDate: {
        type: Date,
        required: true
    },
    /** Dia que foi pago pelo estudante, 
     * @important Este campo não deve retornar fora da API!
     */
    paidedAt: {
        type: Date,
        required: false
    }
}, defaultOptions())

BillingSchema.plugin(MongooseAutoIncrementID.plugin, {modelName: 'Billings', unique: true, field: 'bid'})

module.exports = mongoose.model("Billings", BillingSchema)