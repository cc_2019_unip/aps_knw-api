const mongoose = require('mongoose')
const { SchemaTypes, Types } = require('mongoose')
const { Schema } = require('../database/mongoose.database')
const { defaultOptions } = require('../utils/model.utils')
const { MongooseAutoIncrementID } = require('mongoose-auto-increment-reworked');

const ClassSchema = new Schema({
    description:{
        type: String,
        maxlength: 1000
    },
    /** Início da aula com a Data + Hora do dia */
    startDatetime: { 
        type: Date,
        required: true,
    },
    /** Fim da aula com data + hora do dia 
     * Esse campo sempre vai ser maior que o [startDatetime] e sempre no mesmo dia!
     * Exemplo:
     * startDatetime = 23/08/2019 10:00
     * endDateTime = 23/09/2019 13:00
    */
    endDatetime: {
        type: Date,
        required: true
    },
    /** Propriedade que contém um Array de Ids dos studantes
     * 
     * Olhem esse link de como usar na busca já populando o objeto:
     * @see https://mongoosejs.com/docs/populate.html#population
     */
    students: [{
        type: SchemaTypes.ObjectId,
        required: true,
        ref: 'Students'        
    }],
    experimental: {
        type: Boolean,
        required: false,
        default: false
    }
}, defaultOptions())

ClassSchema.plugin(MongooseAutoIncrementID.plugin, {modelName: 'Classes', unique: true, field: 'cid'})

module.exports = mongoose.model("Classes", ClassSchema)