const mongoose = require('mongoose')
const { Schema } = require('../database/mongoose.database')
const { MongooseAutoIncrementID } = require('mongoose-auto-increment-reworked');
const { cpf } = require('cpf-cnpj-validator')

const StudentSchema = new Schema({
    name: {
        type: String,
        required: true,
        maxlength: 255,
        trim: true
    },
    /** CPF obrigatório, RG não obrigatório */
    cpf: {
        type: String,
        required: true,
        minlength: 11,
        maxlength: 11,
        validate: {
            validator: (value) => cpf.isValid(value),
            message: prop => `O cpf ${prop.value} está inválido` 
        }
    },
    phone: {
        type: String,
        required: true,
        maxlength: 20
    },
    email: {
        type: String,
        required: true,
        trim: true,
        lowercase: true
    },
    rg: {
        type: String,
        minlength: 9,
        maxlength: 9
    }
})

StudentSchema.plugin(MongooseAutoIncrementID.plugin, {modelName: 'Students', unique: true, field: 'sid'})

module.exports = mongoose.model("Students", StudentSchema);